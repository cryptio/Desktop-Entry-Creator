#include "desktopentrycreator.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DesktopEntryCreator desktopEntryCreator;
    desktopEntryCreator.show();

    return a.exec();
}
