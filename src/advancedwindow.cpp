#include "advancedwindow.h"
#include "ui_advancedwindow.h"

AdvancedWindow::AdvancedWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AdvancedWindow)
{
    ui->setupUi(this);
}

AdvancedWindow::~AdvancedWindow()
{
    delete ui;
}

QString AdvancedWindow::getGenericName() const {
    return ui->GenericNameInput->text();
}

bool AdvancedWindow::isDBusActivatable() const {
    return ui->DBusActivatableInput->isChecked();
}

bool AdvancedWindow::isTerminal() const {
    return ui->TerminalInput->isChecked();
}

bool AdvancedWindow::isStartupNotify() const {
    return ui->StartupNotifyInput->isChecked();
}

bool AdvancedWindow::isNoDisplay() const {
    return ui->NoDisplayInput->isChecked();
}

QString AdvancedWindow::getStartupWMClass() const {
    return ui->StartupWMClassEdit->text();
}

QString AdvancedWindow::getMimeType() const {
    return ui->MimeTypeEdit->toPlainText();
}
