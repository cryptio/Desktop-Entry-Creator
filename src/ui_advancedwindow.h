/********************************************************************************
** Form generated from reading UI file 'advancedwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADVANCEDWINDOW_H
#define UI_ADVANCEDWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_AdvancedWindow
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label;
    QCheckBox *DBusActivatableInput;
    QLabel *label_2;
    QCheckBox *TerminalInput;
    QLabel *label_3;
    QCheckBox *StartupNotifyInput;
    QLabel *label_4;
    QLabel *label_5;
    QLineEdit *GenericNameInput;
    QLineEdit *StartupWMClassEdit;
    QLabel *label_6;
    QTextEdit *MimeTypeEdit;
    QLabel *label_7;
    QCheckBox *NoDisplayInput;

    void setupUi(QDialog *AdvancedWindow)
    {
        if (AdvancedWindow->objectName().isEmpty())
            AdvancedWindow->setObjectName(QString::fromUtf8("AdvancedWindow"));
        AdvancedWindow->resize(400, 300);
        buttonBox = new QDialogButtonBox(AdvancedWindow);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(40, 260, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label = new QLabel(AdvancedWindow);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 50, 111, 16));
        DBusActivatableInput = new QCheckBox(AdvancedWindow);
        DBusActivatableInput->setObjectName(QString::fromUtf8("DBusActivatableInput"));
        DBusActivatableInput->setGeometry(QRect(120, 50, 21, 21));
        label_2 = new QLabel(AdvancedWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 80, 61, 15));
        TerminalInput = new QCheckBox(AdvancedWindow);
        TerminalInput->setObjectName(QString::fromUtf8("TerminalInput"));
        TerminalInput->setGeometry(QRect(70, 80, 21, 21));
        label_3 = new QLabel(AdvancedWindow);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 110, 91, 16));
        StartupNotifyInput = new QCheckBox(AdvancedWindow);
        StartupNotifyInput->setObjectName(QString::fromUtf8("StartupNotifyInput"));
        StartupNotifyInput->setGeometry(QRect(100, 110, 21, 21));
        label_4 = new QLabel(AdvancedWindow);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 170, 111, 16));
        label_5 = new QLabel(AdvancedWindow);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(10, 10, 91, 16));
        GenericNameInput = new QLineEdit(AdvancedWindow);
        GenericNameInput->setObjectName(QString::fromUtf8("GenericNameInput"));
        GenericNameInput->setGeometry(QRect(110, 10, 113, 23));
        StartupWMClassEdit = new QLineEdit(AdvancedWindow);
        StartupWMClassEdit->setObjectName(QString::fromUtf8("StartupWMClassEdit"));
        StartupWMClassEdit->setGeometry(QRect(130, 170, 113, 23));
        label_6 = new QLabel(AdvancedWindow);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 210, 91, 16));
        MimeTypeEdit = new QTextEdit(AdvancedWindow);
        MimeTypeEdit->setObjectName(QString::fromUtf8("MimeTypeEdit"));
        MimeTypeEdit->setGeometry(QRect(10, 230, 141, 61));
        label_7 = new QLabel(AdvancedWindow);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(10, 140, 91, 16));
        NoDisplayInput = new QCheckBox(AdvancedWindow);
        NoDisplayInput->setObjectName(QString::fromUtf8("NoDisplayInput"));
        NoDisplayInput->setGeometry(QRect(80, 140, 21, 21));

        retranslateUi(AdvancedWindow);
        QObject::connect(buttonBox, SIGNAL(accepted()), AdvancedWindow, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AdvancedWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(AdvancedWindow);
    } // setupUi

    void retranslateUi(QDialog *AdvancedWindow)
    {
        AdvancedWindow->setWindowTitle(QApplication::translate("AdvancedWindow", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AdvancedWindow", "DBusActivatable:", 0, QApplication::UnicodeUTF8));
        DBusActivatableInput->setText(QString());
        label_2->setText(QApplication::translate("AdvancedWindow", "Terminal:", 0, QApplication::UnicodeUTF8));
        TerminalInput->setText(QString());
        label_3->setText(QApplication::translate("AdvancedWindow", "StartupNotify:", 0, QApplication::UnicodeUTF8));
        StartupNotifyInput->setText(QString());
        label_4->setText(QApplication::translate("AdvancedWindow", "StartupWMClass:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("AdvancedWindow", "GenericName:", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("AdvancedWindow", "MimeType(s):", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("AdvancedWindow", "NoDisplay:", 0, QApplication::UnicodeUTF8));
        NoDisplayInput->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class AdvancedWindow: public Ui_AdvancedWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADVANCEDWINDOW_H
