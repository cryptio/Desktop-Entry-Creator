#ifndef DESKTOPENTRYCREATOR_H
#define DESKTOPENTRYCREATOR_H

#include <QMainWindow>
#include "DesktopEntry.h"
#include "advancedwindow.h"

namespace Ui {
class DesktopEntryCreator;
}

class DesktopEntryCreator : public QMainWindow
{
    Q_OBJECT

public:
    explicit DesktopEntryCreator(QWidget *parent = 0);
    ~DesktopEntryCreator();

private slots:
    void on_CreateButton_clicked();

private:
    // Functions that set their respective field to the value entered within the UI
    void Type_callback();

    void Name_callback();

    void Destination_callback();

    void Comment_callback();

    void Version_callback();

    void Exec_callback();

    void Categories_callback();

    void Path_callback();

    void Icon_callback();

    void GenericName_callback();

    void DBusActivatable_callback();

    void Terminal_callback();

    void StartupNotify_callback();

    void NoDisplay_callback();

    void StartupWMClass_callback();

    void MimeType_callback();

    Ui::DesktopEntryCreator *ui;
    AdvancedWindow* advancedWindow;
    DesktopEntry desktopEntry;
    std::string destinationFile;
};

#endif // DESKTOPENTRYCREATOR_H
