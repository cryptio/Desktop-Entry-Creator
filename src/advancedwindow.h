#ifndef ADVANCEDWINDOW_H
#define ADVANCEDWINDOW_H

#include <QDialog>
#include <vector>

namespace Ui {
class AdvancedWindow;
}

class AdvancedWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AdvancedWindow(QWidget *parent = 0);
    ~AdvancedWindow();

    // Getters
    QString getGenericName() const;

    bool isDBusActivatable() const;

    bool isTerminal() const;

    bool isStartupNotify() const;

    bool isNoDisplay() const;

    QString getStartupWMClass() const;

    QString getMimeType() const;

private:
    Ui::AdvancedWindow *ui;
};

#endif // ADVANCEDWINDOW_H
