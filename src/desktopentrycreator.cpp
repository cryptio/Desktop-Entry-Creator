#include "desktopentrycreator.h"
#include "ui_desktopentrycreator.h"
#include <QTextStream>
#include <QDir>
#include <QFileInfo>
#include "advancedwindow.h"

DesktopEntryCreator::DesktopEntryCreator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DesktopEntryCreator), advancedWindow(new AdvancedWindow), desktopEntry()
{
    ui->setupUi(this);
    // Every time AdvancedButton is clicked(), execute advancedWindow->exec()
    connect(ui->AdvancedButton, SIGNAL(clicked()), advancedWindow, SLOT(exec()));
}

DesktopEntryCreator::~DesktopEntryCreator()
{
    delete ui;
}

void DesktopEntryCreator::Type_callback()
{
    const QString TypeInput = ui->TypeEdit->text();
    QTextStream(stdout) << "TypeInput:" << TypeInput << '\n';
    for (const auto &type : getEntryTypeMapping()) {
        if (type.second == TypeInput.toStdString()) {
            desktopEntry.setType(type.first);
            return;
        }
    }
    QTextStream(stderr) << "Invalid Type\n";    // no matching Type was found
}

void DesktopEntryCreator::Name_callback()
{
    const QString NameInput = ui->NameEdit->text();
    if (NameInput.isEmpty()) {
        QTextStream(stderr) << "Empty field for Name\n";
    }
    QTextStream(stdout) << "NameInput:" << NameInput << '\n';
    desktopEntry.setName(NameInput.toStdString());

}

void DesktopEntryCreator::Destination_callback()
{
    const QString DestinationInput = ui->DestinationEdit->text();
    if (DestinationInput.isEmpty()) {
        QTextStream(stderr) << "Empty field for Destination\n";
    }
    QTextStream(stdout) << "DestinationInput:" << DestinationInput << '\n';
    DesktopEntryCreator::destinationFile = DestinationInput.toStdString();
}

void DesktopEntryCreator::Comment_callback()
{
    const QString CommentInput = ui->CommentEdit->text();
    QTextStream(stdout) << "CommentInput:" << CommentInput << '\n';
    desktopEntry.setComment(CommentInput.toStdString());
}

void DesktopEntryCreator::Version_callback()
{
    const QString VersionInput = ui->VersionEdit->text();
    const float floatVersion = VersionInput.toFloat();
    QTextStream(stdout) << "VersionInput:" << floatVersion << '\n';
    desktopEntry.setVersion(floatVersion);
}

void DesktopEntryCreator::Exec_callback()
{
    const QString ExecInput = ui->ExecEdit->text();
    if (ExecInput.isEmpty()) {
        QTextStream(stderr) << "Empty field for Exec\n";
    }
    QTextStream(stdout) << "ExecInput:" << ExecInput << '\n';
    desktopEntry.setExec(ExecInput.toStdString());
}

void DesktopEntryCreator::Categories_callback()
{
    const QString CategoriesInput = ui->CategoriesEdit->toPlainText();
    std::stringstream ss(CategoriesInput.toStdString());
    std::string buffer;
    std::vector<Category> categories;
    while (std::getline(ss, buffer)) {
        for (const auto &category : getCategoriesMapping()) {
            if (category.second == buffer) {
                categories.push_back(category.first);
                QTextStream(stdout) << QString::fromStdString(buffer) << '\n';
            }
        }
    }
    desktopEntry.setCategories(categories);
}

void DesktopEntryCreator::Path_callback()
{
    const QString PathInput = ui->PathEdit->text();
    if (PathInput.isEmpty()) {
        desktopEntry.setPath("");
        return;
    }

    const QDir dir(PathInput);
    if (dir.exists()) {
        desktopEntry.setPath(PathInput.toStdString());
        QTextStream(stdout) << "PathInput:" << PathInput << '\n';
    } else {
        QTextStream(stderr) << PathInput << " is not a directory\n";
    }
}

void DesktopEntryCreator::Icon_callback()
{
    const QString IconInput = ui->IconEdit->text();
    if (IconInput.isEmpty()) {
        desktopEntry.setIcon("");
        return;
    }

    QFileInfo IconFile = QFileInfo(IconInput);
    const bool IconValid = (IconFile.exists() && IconFile.isFile());    // check specified file path for validity
    if (IconValid) {
        desktopEntry.setIcon(IconInput.toStdString());
        QTextStream(stdout) << "IconInput:" << IconInput << '\n';
    } else {
        QTextStream(stderr) << IconInput << " is not a file\n";
    }
}

void DesktopEntryCreator::GenericName_callback()
{
    const QString GenericNameInput = advancedWindow->getGenericName();
    desktopEntry.setGenericName(GenericNameInput.toStdString());
    QTextStream(stdout) << "GenericName:" << GenericNameInput << '\n';
}

void DesktopEntryCreator::DBusActivatable_callback()
{
    const bool DBusActivatableInput = advancedWindow->isDBusActivatable();
    desktopEntry.setDBusActivatable(DBusActivatableInput);
    QTextStream(stdout) << "DBusActivatable:" << DBusActivatableInput << '\n';
}

void DesktopEntryCreator::Terminal_callback()
{
    const bool TerminalInput = advancedWindow->isTerminal();
    desktopEntry.setTerminal(TerminalInput);
    QTextStream(stdout) << "Terminal:" << TerminalInput << '\n';
}

void DesktopEntryCreator::StartupNotify_callback()
{
    const bool StartupNotifyInput = advancedWindow->isStartupNotify();
    desktopEntry.setStartupNotify(StartupNotifyInput);
    QTextStream(stdout) << "StartupNotify:" << StartupNotifyInput << '\n';
}

void DesktopEntryCreator::NoDisplay_callback()
{
    const bool NoDisplayInput = advancedWindow->isNoDisplay();
    desktopEntry.setNoDisplay(NoDisplayInput);
    QTextStream(stdout) << "NoDisplay:" << NoDisplayInput << '\n';
}

void DesktopEntryCreator::StartupWMClass_callback()
{
    const QString StartupWMClassInput = advancedWindow->getStartupWMClass();
    desktopEntry.setStartupWMClass(StartupWMClassInput.toStdString());
    QTextStream(stdout) << "StartupWMClass:" << StartupWMClassInput << '\n';
}

void DesktopEntryCreator::MimeType_callback()
{
    const QString MimeTypeInput = advancedWindow->getMimeType();

    if (MimeTypeInput.isEmpty()) {
        desktopEntry.setMimeType(std::vector<std::string>());   // set the value to default
        return;
    }

    std::stringstream ss(MimeTypeInput.toStdString());
    std::string buffer;
    std::vector<std::string> mimetypes;
    while (std::getline(ss, buffer)) {
        mimetypes.push_back(buffer);
        QTextStream(stdout) << QString::fromStdString(buffer) << '\n';
    }

    desktopEntry.setMimeType(mimetypes);
}

void DesktopEntryCreator::on_CreateButton_clicked()
{
    Type_callback();
    Name_callback();
    Comment_callback();
    Version_callback();
    Exec_callback();
    Categories_callback();
    Path_callback();
    Icon_callback();
    DBusActivatable_callback();
    Terminal_callback();
    StartupNotify_callback();
    NoDisplay_callback();
    StartupWMClass_callback();
    MimeType_callback();

    Destination_callback();
    createDesktopEntry(DesktopEntryCreator::destinationFile, desktopEntry);
}
