/********************************************************************************
** Form generated from reading UI file 'desktopentrycreator.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DESKTOPENTRYCREATOR_H
#define UI_DESKTOPENTRYCREATOR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DesktopEntryCreator
{
public:
    QWidget *centralWidget;
    QLabel *N;
    QLineEdit *NameEdit;
    QLineEdit *TypeEdit;
    QLabel *TypeLabel;
    QLabel *label;
    QLineEdit *DestinationEdit;
    QLineEdit *CommentEdit;
    QLabel *label_4;
    QLineEdit *ExecEdit;
    QLabel *ExecLabel;
    QPushButton *CreateButton;
    QLabel *label_6;
    QLineEdit *VersionEdit;
    QLabel *label_7;
    QTextEdit *CategoriesEdit;
    QLabel *label_8;
    QLineEdit *PathEdit;
    QLineEdit *IconEdit;
    QLabel *label_9;
    QPushButton *AdvancedButton;
    QMenuBar *menuBar;
    QMenu *menuDesktop_Entry_Creator;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DesktopEntryCreator)
    {
        if (DesktopEntryCreator->objectName().isEmpty())
            DesktopEntryCreator->setObjectName(QString::fromUtf8("DesktopEntryCreator"));
        DesktopEntryCreator->resize(393, 303);
        centralWidget = new QWidget(DesktopEntryCreator);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        N = new QLabel(centralWidget);
        N->setObjectName(QString::fromUtf8("N"));
        N->setGeometry(QRect(10, 50, 61, 15));
        NameEdit = new QLineEdit(centralWidget);
        NameEdit->setObjectName(QString::fromUtf8("NameEdit"));
        NameEdit->setGeometry(QRect(70, 50, 113, 23));
        TypeEdit = new QLineEdit(centralWidget);
        TypeEdit->setObjectName(QString::fromUtf8("TypeEdit"));
        TypeEdit->setGeometry(QRect(70, 10, 113, 23));
        TypeLabel = new QLabel(centralWidget);
        TypeLabel->setObjectName(QString::fromUtf8("TypeLabel"));
        TypeLabel->setGeometry(QRect(10, 10, 61, 15));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 210, 91, 16));
        DestinationEdit = new QLineEdit(centralWidget);
        DestinationEdit->setObjectName(QString::fromUtf8("DestinationEdit"));
        DestinationEdit->setGeometry(QRect(100, 210, 113, 23));
        CommentEdit = new QLineEdit(centralWidget);
        CommentEdit->setObjectName(QString::fromUtf8("CommentEdit"));
        CommentEdit->setGeometry(QRect(80, 90, 113, 23));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 90, 71, 16));
        ExecEdit = new QLineEdit(centralWidget);
        ExecEdit->setObjectName(QString::fromUtf8("ExecEdit"));
        ExecEdit->setGeometry(QRect(70, 170, 113, 23));
        ExecLabel = new QLabel(centralWidget);
        ExecLabel->setObjectName(QString::fromUtf8("ExecLabel"));
        ExecLabel->setGeometry(QRect(10, 170, 51, 16));
        CreateButton = new QPushButton(centralWidget);
        CreateButton->setObjectName(QString::fromUtf8("CreateButton"));
        CreateButton->setGeometry(QRect(331, 210, 51, 23));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(10, 130, 61, 15));
        VersionEdit = new QLineEdit(centralWidget);
        VersionEdit->setObjectName(QString::fromUtf8("VersionEdit"));
        VersionEdit->setGeometry(QRect(70, 130, 113, 23));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(210, 10, 81, 20));
        CategoriesEdit = new QTextEdit(centralWidget);
        CategoriesEdit->setObjectName(QString::fromUtf8("CategoriesEdit"));
        CategoriesEdit->setGeometry(QRect(210, 30, 151, 70));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(210, 120, 41, 16));
        PathEdit = new QLineEdit(centralWidget);
        PathEdit->setObjectName(QString::fromUtf8("PathEdit"));
        PathEdit->setGeometry(QRect(250, 120, 113, 23));
        IconEdit = new QLineEdit(centralWidget);
        IconEdit->setObjectName(QString::fromUtf8("IconEdit"));
        IconEdit->setGeometry(QRect(250, 160, 113, 23));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(210, 160, 41, 16));
        AdvancedButton = new QPushButton(centralWidget);
        AdvancedButton->setObjectName(QString::fromUtf8("AdvancedButton"));
        AdvancedButton->setGeometry(QRect(250, 210, 71, 23));
        DesktopEntryCreator->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DesktopEntryCreator);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 393, 20));
        menuDesktop_Entry_Creator = new QMenu(menuBar);
        menuDesktop_Entry_Creator->setObjectName(QString::fromUtf8("menuDesktop_Entry_Creator"));
        DesktopEntryCreator->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DesktopEntryCreator);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        DesktopEntryCreator->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DesktopEntryCreator);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        DesktopEntryCreator->setStatusBar(statusBar);

        menuBar->addAction(menuDesktop_Entry_Creator->menuAction());

        retranslateUi(DesktopEntryCreator);

        QMetaObject::connectSlotsByName(DesktopEntryCreator);
    } // setupUi

    void retranslateUi(QMainWindow *DesktopEntryCreator)
    {
        DesktopEntryCreator->setWindowTitle(QApplication::translate("DesktopEntryCreator", "DesktopEntryCreator", 0, QApplication::UnicodeUTF8));
        N->setText(QApplication::translate("DesktopEntryCreator", "* Name:", 0, QApplication::UnicodeUTF8));
        TypeLabel->setText(QApplication::translate("DesktopEntryCreator", "* Type:", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("DesktopEntryCreator", "* Destination:", 0, QApplication::UnicodeUTF8));
        DestinationEdit->setText(QString());
        CommentEdit->setText(QString());
        label_4->setText(QApplication::translate("DesktopEntryCreator", "Comment:", 0, QApplication::UnicodeUTF8));
        ExecLabel->setText(QApplication::translate("DesktopEntryCreator", "* Exec:", 0, QApplication::UnicodeUTF8));
        CreateButton->setText(QApplication::translate("DesktopEntryCreator", "Create", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("DesktopEntryCreator", "Version:", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("DesktopEntryCreator", "Categories:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("DesktopEntryCreator", "Path:", 0, QApplication::UnicodeUTF8));
        IconEdit->setText(QString());
        label_9->setText(QApplication::translate("DesktopEntryCreator", "Icon:", 0, QApplication::UnicodeUTF8));
        AdvancedButton->setText(QApplication::translate("DesktopEntryCreator", "Advanced", 0, QApplication::UnicodeUTF8));
        menuDesktop_Entry_Creator->setTitle(QApplication::translate("DesktopEntryCreator", "Desktop Entry Creator", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DesktopEntryCreator: public Ui_DesktopEntryCreator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DESKTOPENTRYCREATOR_H
