/**
 * DesktopEntryCreator
 * DesktopEntry.h
 * Purpose: The interface for the DesktopEntry class. This class was designed using the Keys and information located at
 * https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s06.html
 *
 * @author Encryptio
 * @version 1.0
 *
 */

#ifndef DESKTOPENTRYCREATOR_DESKTOPENTRY_H
#define DESKTOPENTRYCREATOR_DESKTOPENTRY_H

#include <string>
#include <vector>
#include <sstream>
#include <unordered_map>

// Represents the possible types for the Type key
enum class EntryType {
    Application = 1,
    Link,
    Directory
};

// Represents the possible categories for the Categories key
enum class Category {
    AudioVideo,
    Audio,
    Video,
    Development,
    Education,
    Game,
    Graphics,
    Network,
    Office,
    Settings,
    System,
    Utility
};

class DesktopEntry {
public:
    DesktopEntry();

    const EntryType &getType() const;

    const std::string &getName() const;

    const std::string &getGenericName() const;

    const std::string &getComment() const;

    const std::string &getPath() const;

    const std::string &getExec() const;

    const std::string &getIcon() const;

    const std::string &getStartupWMClass() const;

    const float &getVersion() const;

    const bool &isDBusActivatable() const;

    const bool &isTerminal() const;

    const bool &isStartupNotify() const;

    const bool &isNoDisplay() const;

    const std::vector<Category> &getCategories() const;

    const std::vector<std::string> &getMimeType() const;

    void setType(const EntryType &type);

    void setName(const std::string &name);

    void setGenericName(const std::string &genericname);

    void setComment(const std::string &comment);

    void setPath(const std::string &path);

    void setExec(const std::string &exec);

    void setIcon(const std::string &icon);

    void setStartupWMClass(const std::string &startupwmclass);

    void setVersion(const float &version);

    void setDBusActivatable(const bool &dbusactivatable);

    void setTerminal(const bool &terminal);

    void setStartupNotify(const bool &startupnotify);

    void setNoDisplay(const bool &nodisplay);

    void setCategories(const std::vector<Category> &categories);

    void setMimeType(const std::vector<std::string> &mimetype);

private:
    EntryType Type;
    std::string Name, GenericName, Comment, Path, Exec, Icon, StartupWMClass;
    float Version;
    bool DBusActivatable, Terminal, StartupNotify, NoDisplay;
    std::vector<Category> Categories;
    std::vector<std::string> MimeType;

};

/**
 * @brief Provides a mapping of string representations for the Category constants
 * @return Unordered map of {Category, string} pairs
 */
std::unordered_map<Category, std::string> getCategoriesMapping();

/**
 * Provides a mapping of string representations for the EntryType constants
 * @return Unordered map of {EntryType, string} pairs
 */
std::unordered_map<EntryType, std::string> getEntryTypeMapping();

/**
 * @brief Formats a name and a value so it can be properly written to a .desktop file
 * @note Function overloading is used as opposed to templates, because not every type can be treated the same
 * @param key A valid desktop entry key in the form of a string
 * @param value The value(s) for the key
 * @return Formatted string
 */
std::string formatDesktopEntryField(const std::string &key, const std::string &value);

std::string formatDesktopEntryField(const std::string &key, const bool &value);

std::string formatDesktopEntryField(const std::string &key, const float &value);

std::string formatDesktopEntryField(const std::string &key, const std::vector<Category> &values);

std::string formatDesktopEntryField(const std::string &key, const EntryType &value);

std::string formatDesktopEntryField(const std::string &key, const std::vector<std::string> &values);

/**
 * @brief Creates a .desktop file containing the information from a valid DesktopEntry object
 * @param destination The destination file
 * @param DesktopEntry A DesktopEntry object
 */
void createDesktopEntry(const std::string &destination, const DesktopEntry &desktopEntry);

#endif //DESKTOPENTRYCREATOR_DESKTOPENTRY_H
