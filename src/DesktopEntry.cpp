/**
 * DesktopEntryCreator
 * DesktopEntry.cpp
 * Purpose: Provides the implementation of the DesktopEntry class
 *
 * @author Encryptio
 * @version 1.0
 */

#include "DesktopEntry.h"
#include <fstream>
#include <iostream>


// Initializes all of the members with their default values
DesktopEntry::DesktopEntry()
        : Type(), Name(), GenericName(), Comment(), Path(), Exec(), Icon(), StartupWMClass(), Version(),
          DBusActivatable(), Terminal(),
          StartupNotify(), NoDisplay(),
          Categories(), MimeType() {
    ;
}

const EntryType &DesktopEntry::getType() const {
    return Type;
}

const std::string &DesktopEntry::getName() const {
    return Name;
}

const std::string &DesktopEntry::getGenericName() const {
    return GenericName;
}

const std::string &DesktopEntry::getComment() const {
    return Comment;
}

const std::string &DesktopEntry::getPath() const {
    return Path;
}

const std::string &DesktopEntry::getExec() const {
    return Exec;
}

const std::string &DesktopEntry::getIcon() const {
    return Icon;
}

const std::string &DesktopEntry::getStartupWMClass() const {
    return StartupWMClass;
}

const float &DesktopEntry::getVersion() const {
    return Version;
}

const bool &DesktopEntry::isDBusActivatable() const {
    return DBusActivatable;
}

const bool &DesktopEntry::isTerminal() const {
    return Terminal;
}

const bool &DesktopEntry::isStartupNotify() const {
    return StartupNotify;
}

const bool &DesktopEntry::isNoDisplay() const {
    return NoDisplay;
}

const std::vector<Category> &DesktopEntry::getCategories() const {
    return Categories;
}

const std::vector<std::string> &DesktopEntry::getMimeType() const {
    return MimeType;
}

void DesktopEntry::setType(const EntryType &type) {
    DesktopEntry::Type = type;
}

void DesktopEntry::setName(const std::string &name) {
    DesktopEntry::Name = name;
}

void DesktopEntry::setGenericName(const std::string &genericname) {
    DesktopEntry::GenericName = genericname;
}

void DesktopEntry::setComment(const std::string &comment) {
    DesktopEntry::Comment = comment;
}

void DesktopEntry::setPath(const std::string &path) {
    DesktopEntry::Path = path;
}

void DesktopEntry::setExec(const std::string &exec) {
    DesktopEntry::Exec = exec;
}

void DesktopEntry::setIcon(const std::string &icon) {
    std::ifstream iconFile(icon.c_str());
    if (!iconFile) {
        std::cerr << "File does not exist\n";
    }

    DesktopEntry::Icon = icon;
}

void DesktopEntry::setStartupWMClass(const std::string &startupwmclass) {
    DesktopEntry::StartupWMClass = startupwmclass;
}

void DesktopEntry::setVersion(const float &version) {
    DesktopEntry::Version = version;
}

void DesktopEntry::setDBusActivatable(const bool &dbusactivatable) {
    DesktopEntry::DBusActivatable = dbusactivatable;
}

void DesktopEntry::setTerminal(const bool &terminal) {
    DesktopEntry::Terminal = terminal;
}

void DesktopEntry::setStartupNotify(const bool &startupnotify) {
    DesktopEntry::StartupNotify = startupnotify;
}

void DesktopEntry::setNoDisplay(const bool &nodisplay) {
    DesktopEntry::NoDisplay = nodisplay;
}

void DesktopEntry::setCategories(const std::vector<Category> &categories) {
    DesktopEntry::Categories = categories;
}

void DesktopEntry::setMimeType(const std::vector<std::string> &mimetype) {
    DesktopEntry::MimeType = mimetype;
}

std::unordered_map<Category, std::string> getCategoriesMapping() {
    std::unordered_map<Category, std::string> table({
                                                            {Category::AudioVideo,  "AudioVideo"},
                                                            {Category::Audio,       "Audio"},
                                                            {Category::Video,       "Video"},
                                                            {Category::Development, "Development"},
                                                            {Category::Education,   "Education"},
                                                            {Category::Game,        "Game"},
                                                            {Category::Graphics,    "Graphics"},
                                                            {Category::Network,     "Network"},
                                                            {Category::Office,      "Office"},
                                                            {Category::Settings,    "Settings"},
                                                            {Category::System,      "System"},
                                                            {Category::Utility,     "Utility"}
                                                    });
    return table;
};

std::unordered_map<EntryType, std::string> getEntryTypeMapping() {
    std::unordered_map<EntryType, std::string> table({
                                                             {EntryType::Application, "Application"},
                                                             {EntryType::Directory,   "Directory"},
                                                             {EntryType::Link,        "Link"}
                                                     });

    return table;
}

std::string formatDesktopEntryField(const std::string &name, const std::string &value) {
    if ((name.empty()) || (value.empty())) return "";

    std::stringstream ss;
    ss << name << '=' << value << '\n';

    return ss.str();
}

std::string formatDesktopEntryField(const std::string &name, const bool &value) {
    std::string value_result;
    if (value) {
        value_result = "true";
    } else {
        value_result = "false";
    }
    std::stringstream ss;
    ss << name << '=' << value_result << '\n';

    return ss.str();
}

std::string formatDesktopEntryField(const std::string &name, const std::vector<Category> &values) {
    std::stringstream ss;
    std::stringstream unpacked_values;
    for (const auto &category : values) {
        unpacked_values << getCategoriesMapping()[category] << ';';
    }
    ss << name << '=' << unpacked_values.str() << '\n';

    return ss.str();
}

std::string formatDesktopEntryField(const std::string &name, const EntryType &value) {
    std::stringstream ss;
    ss << name << '=' << getEntryTypeMapping()[value] << '\n';

    return ss.str();
}

std::string formatDesktopEntryField(const std::string &name, const float &value) {
    if ((name.empty()) || (value <= 0)) return "";

    std::stringstream ss;
    ss << name << '=' << value << '\n';
    return ss.str();
}

std::string formatDesktopEntryField(const std::string &name, const std::vector<std::string> &values) {
    if ((name.empty()) || (values.empty())) return "";

    std::stringstream ss;
    std::stringstream unpacked_values;
    for (const auto &value : values) {
        unpacked_values << value << ';';
    }
    ss << name << '=' << unpacked_values.str() << '\n';
    return ss.str();
}

void createDesktopEntry(const std::string &destination, const DesktopEntry &desktopEntry) {
    const std::string groupheader = "[Desktop Entry]\n";
    const std::string name = formatDesktopEntryField("Name", desktopEntry.getName());
    const std::string genericname = formatDesktopEntryField("GenericName", desktopEntry.getGenericName());
    const std::string comment = formatDesktopEntryField("Comment", desktopEntry.getComment());
    const std::string path = formatDesktopEntryField("Path", desktopEntry.getPath());
    const std::string exec = formatDesktopEntryField("Exec", desktopEntry.getExec());
    const std::string icon = formatDesktopEntryField("Icon", desktopEntry.getIcon());
    const std::string startupwmclass = formatDesktopEntryField("StartupWMClass", desktopEntry.getStartupWMClass());
    const std::string version = formatDesktopEntryField("Version", desktopEntry.getVersion());
    const std::string type = formatDesktopEntryField("Type", desktopEntry.getType());
    const std::string dbusactivatable = formatDesktopEntryField("DBusActivatable", desktopEntry.isDBusActivatable());
    const std::string terminal = formatDesktopEntryField("Terminal", desktopEntry.isTerminal());
    const std::string startupnotify = formatDesktopEntryField("StartupNotify", desktopEntry.isStartupNotify());
    const std::string nodisplay = formatDesktopEntryField("NoDisplay", desktopEntry.isNoDisplay());
    const std::string categories = formatDesktopEntryField("Categories", desktopEntry.getCategories());
    const std::string mimetype = formatDesktopEntryField("MimeType", desktopEntry.getMimeType());

    const bool execEmpty = desktopEntry.getExec().empty();

    if ((desktopEntry.getType() == EntryType::Application) && (execEmpty)) {
        std::cerr << "Type Application requires Exec field\n";
    }
    if((!desktopEntry.isDBusActivatable()) && (execEmpty)) {
        std::cerr << "Exec must be defined if DBusActivatable is not set to true\n";
    }

    std::ofstream desktopFile(destination.c_str());
    if (!desktopFile) {
        perror("Error opening destination file");
    }
    desktopFile << groupheader << name << genericname << comment << path << exec << icon << startupwmclass << version
                << type << terminal << dbusactivatable << startupnotify << nodisplay << categories << mimetype << '\n';


}
