## Compilation
In order to compile the files into a single binary file, you can use qmake.
```
cd /folder/of/files
qmake -project
qmake
make		
```
You can then proceed to run the executable file. From the command-line:
```
./DesktopEntryCreator
```

If you want to use this project as an API, you can include the file DesktopEntry.h in your source code and then use that instead. For compilation, you would include DesktopEntry.cpp in your compilation command:
```
g++ -o (YourProject) {yourprojectfiles} DesktopEntry.cpp
