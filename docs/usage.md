## Usage
The GUI was designed to provide a simple interface which lets you create desktop entries with ease. The fields with an asterisk (*) prefix are required in order to make a valid .desktop file.  For extensive coverage of all keys and fields, please see https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s06.html. 

*Note*: In the specification, it states that `Exec` is not required. However, in the description it says:
> The `Exec` key is required if `DBusActivatable` is not set to `true`. Even if `DBusActivatable` is `true`, `Exec` should be specified for compatibility with implementations that do not understand `DBusActivatable`.

For this reason, I have made `Exec` necessary.
